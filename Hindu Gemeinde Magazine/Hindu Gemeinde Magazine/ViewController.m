//
//  ViewController.m
//  Hindu Gemeinde Magazine
//
//  Created by Verbi IT on 5/14/21.
//  Copyright © 2021 Hindu Gemeinde Berlin. All rights reserved.
//

#import "ViewController.h"
@import FirebaseDatabase;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    auto ref = Database.database().reference();
    ref.child(".").observeDingleEvent()
    {
        (snapshot) in snapshot.value
    }


@end
