//
//  main.m
//  Hindu Gemeinde Magazine
//
//  Created by Verbi IT on 5/14/21.
//  Copyright © 2021 Hindu Gemeinde Berlin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
    return 0;
}
